unit ChangeCRC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfrmChangeCRC = class(TForm)
    btnUpdate: TButton;
    btnClose: TButton;
    lblCRC: TLabel;
    txtCRC: TEdit;
    txtInfo: TLabel;
    procedure txtCRCChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmChangeCRC: TfrmChangeCRC;

implementation

uses
  Lang;

{$R *.dfm}

procedure TfrmChangeCRC.txtCRCChange(Sender: TObject);
begin

  btnUpdate.Enabled := StrLen(PChar(txtCRC.Text)) > 0;

end;

end.
